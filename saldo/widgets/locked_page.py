# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gettext import gettext as _
from gi.repository import Adw, Gtk, GLib


@Gtk.Template(resource_path='/org/tabos/saldo/ui/locked_page.ui')
class LockedPage(Adw.Bin):
    __gtype_name__ = 'LockedPage'

    _password_entry = Gtk.Template.Child()
    _password_button = Gtk.Template.Child()
    _spinner_stack = Gtk.Template.Child()
    spinner = Gtk.Template.Child()

    def __init__(self, window):
        super().__init__()

        self._window = window

    def do_realize(self):    # pylint: disable=arguments-differ
        Gtk.Widget.do_realize(self)
        self._password_entry.grab_focus()

    @Gtk.Template.Callback()
    def _on_password_button_clicked(self, button):
        if not self._window.backend.is_safe_password_valid(self._password_entry.get_text()):
            self._window._send_notification(_("Failed to Unlock Safe"))
            self._password_entry.add_css_class("error")
            self._stop_progress()

            return

        GLib.idle_add(self._load_database)

    def _start_progress(self):
        self._password_entry.set_sensitive(False)
        self._password_button.set_sensitive(False)
        self.spinner.start()
        self._spinner_stack.set_visible_child_name('spinner')

    def _stop_progress(self):
        self._password_entry.set_sensitive(True)
        self._password_button.set_sensitive(True)
        self.spinner.stop()
        self._spinner_stack.set_visible_child_name('image')

    def _load_database(self):
        self._start_progress()
        self._window.backend.load_database()
        self._stop_progress()
        self._password_entry.set_text("")
        self._password_entry.remove_css_class("error")
        self._window.view = self._window.View.UNLOCKED

    @Gtk.Template.Callback()
    def _on_password_entry_activate(self, _):
        self._password_button.activate()
