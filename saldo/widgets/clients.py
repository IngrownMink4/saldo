# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Adw, Gtk, GLib, GObject
from saldo.widgets.listviewstore import ListViewListStore

from saldo.backend.backend import BankingBackend


class BankElem(GObject.GObject):
    def __init__(self, name: str, blz: str, bic: str, logo: str, city: str, server: str):
        super(BankElem, self).__init__()
        self.name = name
        self.blz = blz
        self.bic = bic
        self.city = city

        self.logo = logo
        self.server = server

    def __repr__(self):
        return f'BankElem(name: {self.name} blz: {self.blz})'


class BankFilter(Gtk.Filter):
    __gtype_name__ = 'BankFilter'

    def __init__(self, search: str):
        self.search = search.lower()
        super(BankFilter, self).__init__()

    def do_match(self, item) -> bool:
        return self.search in item.name.lower() or \
            self.search in item.city.lower() or \
            item.blz.lower().startswith(self.search) or \
            item.bic.lower().startswith(self.search)


class BankListView(ListViewListStore):
    def __init__(self, win: Gtk.ApplicationWindow, backend: BankingBackend):
        super(BankListView, self).__init__(BankElem)
        self.win = win
        self.set_show_separators(True)

        for db in backend._client_db['databases']:
            blz = str(db['blz'])
            bic = str(db['bic'])
            name = GLib.markup_escape_text(str(db['institute']))
            logo = str(db['logo'])
            city = str(db['city'])
            server = str(db['url'])

            self.add(BankElem(name, blz, bic, logo, city, server))

    def factory_setup(self, widget: Gtk.ListView, item: Gtk.ListItem):
        row = Adw.ActionRow()
        item.set_child(row)

    def factory_bind(self, widget: Gtk.ListView, item: Gtk.ListItem):
        row = item.get_child()
        data = item.get_item()

        if data.city.lower() in data.name.lower():
            row.set_title(data.name)
        else:
            row.set_title(data.name + " (" + data.city + ")")
        row.set_subtitle(data.blz)
        row.set_icon_name(data.logo)
        item.set_child(row)

    def factory_unbind(self, widget: Gtk.ListView, item: Gtk.ListItem):
        pass

    def factory_teardown(self, widget: Gtk.ListView, item: Gtk.ListItem):
        pass

    def selection_changed(self, widget, ndx: int):
        pass


@Gtk.Template(resource_path='/org/tabos/saldo/ui/clients.ui')
class Clients(Gtk.Dialog):
    __gtype_name__ = 'Clients'

    _stack = Gtk.Template.Child()
    _next_button = Gtk.Template.Child()
    _bank_row = Gtk.Template.Child()
    _user_entry = Gtk.Template.Child()
    _password_entry = Gtk.Template.Child()
    _bank_search_entry = Gtk.Template.Child()
    _bank_scrolled_window = Gtk.Template.Child()

    def __init__(self, backend: BankingBackend, **kwargs) -> None:
        super().__init__(**kwargs)

        self._backend = backend
        self._institute = None

        self.view = BankListView(self, self._backend)
        self.view.add_css_class("card")
        self.view.connect('activate', self.on_view_activated)
        self._bank_scrolled_window.set_child(self.view)
        self.info = None

        self.connect('response', self._on_response)

    def on_view_activated(self, view: BankListView, pos: int):
        self._next_button.activate()

    @Gtk.Template.Callback()
    def _on_bank_search_changed(self, _) -> None:
        bank = self._bank_search_entry.get_text()

        if len(bank) > 2:
            self.view.filtered.set_filter(BankFilter(bank))
        else:
            self.view.filtered.set_filter(None)

    @Gtk.Template.Callback()
    def _on_bank_credentials_changed(self, edit):
        if len(self._user_entry.get_text()) and len(self._password_entry.get_text()):
            self._next_button.set_sensitive(True)
        else:
            self._next_button.set_sensitive(False)

    def _on_response(self, accounts, ret) -> None:
        if ret == Gtk.ResponseType.APPLY and self._stack.get_visible_child_name() == 'bank_selection':
            idx = self.view.model.get_selection().get_nth(0)
            self.info = self.view.model.get_item(idx)

            self._bank_row.set_title(self.info.name)
            self._bank_row.set_subtitle(self.info.blz)
            self._bank_row.set_icon_name(self.info.logo)

            self._stack.set_visible_child_name('credentials')
            self._next_button.set_sensitive(False)
            return

        if ret == Gtk.ResponseType.APPLY:
            self._backend.add_client(
                self._user_entry.get_text(),
                self._password_entry.get_text(),
                self.info.server,
                self.info.blz)

            self._backend.refresh_accounts()

        self.destroy()
