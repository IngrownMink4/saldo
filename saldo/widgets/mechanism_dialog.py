# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk


@Gtk.Template(resource_path='/org/tabos/saldo/ui/mechanism_dialog.ui')
class MechanismDialog(Gtk.Dialog):
    __gtype_name__ = 'MechanismDialog'

    _listbox = Gtk.Template.Child()
    _select_button = Gtk.Template.Child()

    def __init__(self, mechanisms, **kwargs):
        super().__init__(**kwargs)

        self._listbox.set_header_func(self._listbox_header)
        self._listbox.connect('row-selected', self._on_row_selected)
        self.mechanism = 0

        self._select_button.add_css_class('suggested-action')

        for i, mechanism in enumerate(mechanisms):
            row = Gtk.ListBoxRow()
            label = Gtk.Label(label=mechanism[1].name)
            label.set_margin_start(6)
            label.set_margin_end(6)
            label.set_margin_top(6)
            label.set_margin_bottom(6)
            setattr(row, 'id', i)
            row.set_child(label)
            self._listbox.append(row)

    @Gtk.Template.Callback()
    def _on_row_selected(self, _, row):
        idx = getattr(row, 'id')
        self.mechanism = idx

    @Gtk.Template.Callback()
    def _on_row_activated(self, _, row):
        self._select_button.activate()

    def _listbox_header(self, row, before):
        if before and not row.get_header():
            row.set_header(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL))
        elif row.get_header():
            row.set_header(None)
